import utfpr.ct.dainf.if62c.exemplos.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {
 public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz(); 
        m[0][0] = 0.0;
        m[0][1] = 1.0;
        m[1][0] = 2.0;
        m[1][1] = 3.0;
        m[2][0] = 4.0;
        m[2][1] = 5.0;
        
        
        Matriz auxsoma = new Matriz(3, 2);
        double [][]soma = auxsoma.getMatriz();
        soma[0][0] = 1.0;
        soma[0][1] = 3.0;
        soma[1][0] = 5.0;
        soma[1][1] = 7.0;
        soma[2][0] = 9.0;
        soma[2][1] = 11.0;

        Matriz transp = orig.getTransposta();

        Matriz s = orig.soma(auxsoma);
        
        Matriz transps = auxsoma.getTransposta();
        Matriz mult = orig.prod(transps);
        
        
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);

        System.out.println("Matriz soma: " + s);
        System.out.println("Matriz produto: " + mult);
    }
}
